// pages/wxml/index2.js
const faultconfig = require('../../utils/faultconfig.js');
var helloData={
  faultConfigList:[
    {
      "code": "故障代码",
      "desc_zh_CN": "故障描述",
      "solution_zh_CN": "解决办法(中文简体)",
      "desc_zh_TW": "故障描述（中文繁体-台湾）",
      "desc_en_": "故障描述（英语）",
      "solution_en_": "解决办法(英语)",
      "desc_ru_": "故障描述（俄罗斯语）",
      "desc_ar_": "故障描述（阿拉伯语）",
      "desc_es_": "故障描述（西班牙语）",
      "desc_ko_": "故障描述（韩语）",
      "solution_ko_": "解决办法(韩语)",
      "desc_tr_": "故障描述（土耳其语）",
      "desc_fr_": "故障描述（法语）",
      "solution_fr_": "解决办法(法语)",
      "desc_uk_": "故障描述（乌克兰语）",
      "solution_uk_": "解决办法（乌克兰语）",
      "desc_de_": "故障描述（德语）",
      "solution_de_": "解决办法（德语）",
      "desc_ja_": "故障描述（日语）",
      "solution_ja_": "解决办法（日语）",
      "desc_th_": "故障描述（泰语）",
      "solution_th_": "解决办法（泰语）",
      "desc_sk_": "故障描述（斯洛伐克语）",
      "solution_sk_": "解决办法（斯洛伐克语）",
      "desc_cs_": "故障描述（捷克语）",
      "solution_cs_": "解决办法（捷克语）",
      "desc_pl_": "故障描述（波兰语）",
      "solution_pl_": "解决办法（波兰语）"
    }
  ]
}
Page({



  /**
   * 页面的初始数据
   */
  data: {
    message:'hahah',
    name:'name33',
    flag:false,
    array:['1','2','1','4'],
    array2:[
      {id:0,name:'0'},
      {id:1,name:'1'},
      {id:2,name:'2'},
      {id:3,name:'3'}
    ],
    faultConfigList:helloData.faultConfigList
  },
  changeName:function(e){
    this.setData({
      name:'heihei',
      flag:!this.data.flag
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      faultconfig.test();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})